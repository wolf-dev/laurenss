<html>
    <head>
        <title>CMS - @yield('title')</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>--}}
        {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>--}}
        <script src="/js/app.js"></script>
        <script src="/js/tinymce/tinymce.js"></script>
        <script src="/js/slick/slick.min.js"></script>
        <script src="/js/fancybox/jquery.fancybox.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/css/admin.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
        <div class="float-bar">
            <div class="float-inner">
                <div class="user">
                    ingelogd als {{Auth::user()->name}}
                </div>
                <a href="{{ route('logout') }}" class="logout">
                    Uitloggen
                    <i class="material-icons">directions_walk</i>
                </a>
            </div>
        </div>
        <nav>
            <a href="/admin" class="@if(Request::is('admin')) active @endif">
                <i class="material-icons">dashboard</i>
                Dashboard
            </a>
            <a href="/admin/paginas" class="@if(Request::is('admin/paginas*')) active @endif">
                <i class="material-icons">format_align_left</i>
                Pagina's
            </a>
            <a href="/admin/afbeeldingen" class="@if(Request::is('admin/afbeeldingen*')) active @endif">
                <i class="material-icons">image</i>
                Afbeeldingen
            </a>
        </nav>

        <div class="content">
            <div class="top-content">
                <h1>@yield('title')</h1>
                <div class="controls">
                    @yield('controls')
                </div>
            </div>
            @yield('content')
        </div>

        <script>
            $('.slick-slider').slick();

            $('.close-button').on('click', function() {
                $('.second').hide();
                $('.first').show();
                $('.controls .save-button').hide();
                $('.controls .add-button').show();
                $(this).hide();
            });
        </script>
    @yield('scripts')
    </body>
</html>