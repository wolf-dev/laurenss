{{ csrf_field() }}
<div class="form-group row">
    <div class="col-7">
        <input type="text" name="name" class="form-control" value="{{$image->name ?? ''}}" placeholder="Naam *">
    </div>
    <div class="col-5">
        <select name="extension" class="custom-select">
            @if(isset($image->extension))
                <option value="jpg" {{$image->extension == 'jpg' ? 'selected' : $image->extension == 'jpeg' ? 'selected' : ''}}>.jpg</option>
                <option value="png" {{$image->extension == 'png' ? 'selected' : ''}}>.png</option>
            @else
                <option value="jpg">.jpg</option>
                <option value="png">.png</option>
            @endif
        </select>
    </div>
</div>
<div class="form-group">
    <input id="image-alt" type="text" name="alt" class="form-control" value="{{$image->alt ?? ''}}" placeholder="Alt *">
</div>
<div class="form-group image-div">
    <img class="placeholder" src="" alt="">
</div>
<div class="form-group hidden-input">
    <input type="file" name="image" class="image" accept="image/jpeg, image/png">
</div>
@if(isset($image))
    <div class="form-group image-div">
        <img class="original edit-image-input" data-edit="{{$image->id}}" src="{{asset('storage/images/'.$image->name.'.'.$image->extension).'?'.time()}}" alt="" id=""/>
    </div>
    <div class="form-group hidden-input">
        <input type="file" name="image" class="image" id="edit-image-{{$image->id}}" accept="image/jpeg, image/png">
    </div>
@endif
