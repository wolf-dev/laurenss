@extends('admin.layouts.master')

@section('title', "Afbeeldingen")
@section('controls')
    <button class="transparent-button save-button"><i class="material-icons">done</i></button>
    <button class="transparent-button close-button"><i class="material-icons">close</i></button>
    <button class="transparent-button add-button"><i class="material-icons">add</i></button>
@endsection

@section('content')

    <div class="add-image second">
        <form class="add-form" action="{{url("/admin/afbeeldingen/upload")}}" method="POST" enctype="multipart/form-data">
            @include('admin.images.parts.form')
        </form>
    </div>

    @foreach($images as $image)
        <div class="edit-image second" id="edit-{{$image->id}}">
            <form class="edit-form" action="{{url("/admin/afbeeldingen/$image->id/update")}}" method="POST" enctype="multipart/form-data">
                @include('admin.images.parts.form')
            </form>
        </div>
    @endforeach

    <div class="images first">
        <div class="row">
            <?php $counter = 0; ?>

            @foreach($images as $image)
                @if($counter % 4 == 0)
                    </div>
                    <div class="row">
                @endif
                <div class="col-3 item">
                    <div class="item-inner">
                        <div class="edit-bar">
                            <button class="more-button" type="button" data-toggle-id="more-{{$image->id}}"><i class="material-icons">more_horiz</i></button>
                        </div>
                        <div class="more-toggle" id="more-{{$image->id}}">
                            <div class="more-menu">
                                <button class="more-menu-item edit-button" data-edit="{{$image->id}}"><i class="material-icons">edit</i></button>
                                <button class="more-menu-item" onclick="window.location.href='{{url("/admin/afbeeldingen/$image->id/destroy")}}'"><i class="material-icons">delete</i></button>
                            </div>
                        </div>
                        <a href="{{asset('storage/images/'.$image->name.'.'.$image->extension).'?'.time()}}" data-fancybox="group">
                            <div class="thumb-holder" id="{{$image->id}}" style="background: url('{{asset('storage/images/thumbnails/'.$image->name.'.'.$image->extension).'?'.time()}}') no-repeat 100% 100%;"></div>
                        </a>
                    </div>
                </div>
                <?php $counter += 1; ?>
            @endforeach
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $("[data-fancybox]").fancybox({
            buttons : [
                'fullScreen',
                'thumbs',
                'download',
                'close'
            ]
        });

        $('.more-button').on('click', function() {
            var dataElement = $('#'+$(this).data('toggle-id'));

            if(dataElement.is(':visible')) {
                dataElement.hide();
            } else {
                $('.more-toggle').hide();
                dataElement.show();
            }
        });

        $(document).on('click', function(e) {
            var target = $(e.target);
            if(!target.closest('.item').length) {
                $('.more-toggle').hide();
            }
        });

        //ADD IMAGE
        $('.add-button').on('click', function() {
            var addImageInput = $('.add-image .hidden-input input');
            if(!addImageInput.val()) {
                addImageInput.trigger('click');
            } else {
                openAddImage();
            }
        });

        $(".add-image input.image").change(function() {
            readImg(this, 'add');

            openAddImage();
        });

        //EDIT IMAGE
        $('.edit-button').on('click', function() {
            openEditImage($(this).data('edit'));
        });

        $('.edit-image-input').on('click', function() {
            $('#edit-image-'+$(this).data('edit')).trigger('click');
        });

        $(".edit-image input.image").change(function() {
            readImg(this, 'edit');
        });

        function openAddImage() {
            $('.images').hide();
            $('.add-image').show();
            $('.controls .add-button').hide();
            $('.close-button').show();
            $('.save-button').show().on('click', function() {
                $('.add-form').submit();
            });
        }

        function openEditImage(id) {
            $('.images').hide();
            $('#edit-'+id).show();
            $('.controls .add-button').hide();
            $('.close-button').show();
            $('.save-button').show().on('click', function() {
                $('#edit-'+id+' .edit-form').submit();
            });
        }

        function readImg(input, method) {
            if (input.files && input.files[0]) {
                var reader = new FileReader(),
                    placeholder = $('.'+method+'-image img.placeholder'),
                    original = $('.'+method+'-image img.original'),
                    extension = input.files[0].name.split('.').pop().toLowerCase();

                if(extension == 'jpeg') {
                    extension = 'jpg';
                }

                $('.'+method+'-image select#extension').val(extension);

                reader.onload = function(e) {
                    original.hide();
                    placeholder.show();
                    placeholder.attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection