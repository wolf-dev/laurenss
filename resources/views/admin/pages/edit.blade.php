@extends('admin.layouts.master')

@section('title', "Pagina bewerken ($page->title)")

@section('content')

	<ul class="nav nav-tabs" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="tab" href="#content" role="tab">Content</a>
	  	</li>
	  	<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#meta" role="tab">Meta</a>
	  	</li>
	</ul>

	<div class="image-center">
		<div class="image-center-inner">
			<div class="row">
                <?php $counter = 0; ?>

				@foreach($images as $image)
					@if($counter % 4 == 0 && $counter != 0)
						</div>
						<div class="row">
					@endif

					<div class="col-3">
						<div class="image-center-image" data-image-id="{{$image->id}}" style="background: url('{{asset('storage/images/thumbnails/'.$image->name.'.'.$image->extension).'?'.time()}}') no-repeat 100% 100%;"></div>
					</div>

					<?php $counter += 1; ?>
				@endforeach
			</div>
		</div>
	</div>

	<div class="tab-content">
		<div class="tab-pane active" id="content" role="tabpanel">
			<div class="row">
				<?php $widthSum = 0; ?>
				@foreach($blocks as $block)
					<?php $widthSum += $block->width; ?>
					@if($block->blockable_type == 'text')
						<div class="block text-block col-md-{{$block->width}}">
							<form action="{{url("/admin/blocks/$block->id/update")}}" method="POST">
								{{ csrf_field() }}
								<div class="form-group">
									<textarea class="form-control tinymce" name="content" id="content-{{$block->id}}">{!!$block->blockable->content ?? ''!!}</textarea>
								</div>
								<div class="form-group">
									<input type="submit" value="Opslaan" class="full-width">
								</div>
							</form>
						</div>
					@elseif($block->blockable_type == 'image')
						<div class="block image-block col-md-{{$block->width}}">
							<div class="editable-image">
								@if($block->blockable_id)
									<img src="{{asset('storage/images/'.$block->blockable->name.'.'.$block->blockable->extension).'?'.time()}}" alt="" id=""/>
									<div class="image-edit-hover">
										<div class="buttons" data-block-id="{{$block->id}}">
											<button class="transparent-button edit-button"><i class="material-icons">edit</i></button>
											<button class="transparent-button delete-button"><i class="material-icons">delete</i></button>
										</div>
									</div>
								@else
									<button class="transparent-button add-image add-block-image" data-block-id="{{$block->id}}">
										<i class="material-icons">add_circle_outline</i>
									</button>
								@endif
							</div>
						</div>

					{{--@elseif($block->blockable_type == 'video')--}}
						{{--<div class="block video-block col-md-{{$block->width}}">--}}
							{{--<form action="">--}}
								{{--{{ csrf_field() }}--}}
								{{--<label for="video">Video</label>--}}
								{{--<div class="form-group">--}}
									{{--<input type="text" name="video" class="form-control" value="{{$block->blockable->url}}">--}}
								{{--</div>--}}
								{{--<div class="form-group">--}}
									{{--<input type="submit" value="Opslaan" class="full-width">--}}
								{{--</div>--}}
							{{--</form>--}}
						{{--</div>--}}
					@elseif($block->blockable_type = 'slider')
						<div class="block slider-block col-md-{{$block->width}}">
							<div style="height: {{$block->blockable->height}}px;" class="slick-slider" data-slick='{"slidesToScroll": 1, "variableWidth": true}'>
								<button class="transparent-button add-image add-slide" data-slider-id="{{$block->blockable_id}}">
									<i class="material-icons">add_circle_outline</i>
								</button>
								@foreach($block->blockable->slides as $slide)
									<div class="editable-image">
										<img class="" src="{{asset("storage/images/thumbnails/".$slide->image->name.'.'.$slide->image->extension)}}" alt="">
										<div class="image-edit-hover">
											<div class="buttons" data-slide-id="{{$slide->id}}">
												<button class="transparent-button edit-button"><i class="material-icons">edit</i></button>
												<button class="transparent-button delete-button"><i class="material-icons">delete</i></button>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					@endif
					@if($widthSum == 12)
						</div>
						<div class="space-20"></div>
						<div class="row">
						<?php $widthSum = 0; ?>
					@endif
				@endforeach
			</div>
		</div>
		<div class="tab-pane" id="meta" role="tabpanel">
			Metadata
		</div>
	</div>

@endsection

@section('scripts')
	<script>
		var block = 0,
            slider = 0,
			slide = 0,
			image = 0;

		$('.add-block-image').on('click', function() {
            slider = 0;
            slide = 0;
            block = $(this).data('block-id');
            $('.image-center').show();
		});

		$('.image-block .edit-button').on('click', function() {
		    slider = 0;
		    slide = 0;
		    block = $(this).parent('.buttons').data('block-id');
		    $('.image-center').show();
		});

		$('.image-block .delete-button').on('click', function() {
		    block = $(this).parent('.buttons').data('block-id');
		    window.location.href = '{{url('')}}/admin/blocks/'+block+'/delete-image';
		});

		$('.add-slide').on('click', function() {
		    block = 0;
		    slide = 0;
		    slider = $(this).data('slider-id');
            $('.image-center').show();
		});

		$('.slider-block .edit-button').on('click', function() {
		    block = 0;
		    slider = 0;
		    slide = $(this).parent('.buttons').data('slide-id');
		    $('.image-center').show();
		});

		$('.slider-block .delete-button').on('click', function() {
		    slide = $(this).parent('.buttons').data('slide-id');
            window.location.href = '{{url('')}}/admin/slides/'+slide+'/delete';
        });

		$('.image-center-image').on('click', function() {
		    image = $(this).data('image-id');

		    if(block !== 0) {
		        window.location.href = '{{url('')}}/admin/blocks/'+block+'/change-image/'+image;
			} else if(slider !== 0) {
		        window.location.href = '{{url('')}}/admin/sliders/'+slider+'/add-slide/'+image;
			} else if(slide !== 0) {
		        window.location.href = '{{url('')}}/admin/slides/'+slide+'/change-image/'+image;
			}
		});

		$('.image-center').on('click', function(e) {
            if (!$(e.target).is(".image-center-inner") && !$(e.target).is(".image-center-image")) {
                $('.image-center').hide();
            }
		});
	</script>
	<script>
        tinymce.init({
            selector: '.tinymce',
            branding: false
        });
	</script>
	<script>
		var imageBlockImg = $('.editable-image');

        imageBlockImg.mouseenter(function() {
            $(this).find('.image-edit-hover').toggle();
		}).mouseleave(function() {
            $(this).find('.image-edit-hover').toggle();
		});
	</script>
@endsection