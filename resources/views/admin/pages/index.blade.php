@extends('admin.layouts.master')

@section('title', "Pagina's")

@section('content')

	<table class="table">
		<thead>
			<tr>
				<th>Titel</th>
				<th>Description</th>
				<th>Wijzigen</th>
			</tr>
		</thead>
		<tbody>
			@foreach($pages as $page)
				<tr>
					<td>{{$page->title}}</td>
					<td>{{$page->description}}</td>
					<td><a href="/admin/paginas/{{$page->slug}}/edit"><i class="material-icons">create</i></a></td>
				</tr>
			@endforeach
		</tbody>
	</table>

@endsection