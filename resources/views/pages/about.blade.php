@extends('layouts.app')

@section('title', $page->title)

@section('content')

    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-auto@m uk-text-center">
                <div>
                    <div class="uk-card uk-card-body"><h1>ABOUT<span class="period">.</span></h1></div>
                </div>
            </div>
            <div class="uk-width-expand@m uk-card uk-card-body">
                <h2>Hey!</h2>
                <p>Ik ben Laurens Smid, ik ben 23 jaar oud en ik woon in Rotterdam.</p>

                <p>
                Ik doe de HBO opleiding Creative Media en Game Technologies (CMGT) aan de Hogeschool Rotterdam, momenteel in het tweede leerjaar.
                </p>

                <p>
                In mijn vrije tijd ben ik veel bezig met het maken van websites en applicaties, ook ben ik constant op zoek naar nieuwe ideeën en inspiratie.
                </p>
            </div>
        </div>
    </div>

@endsection