@extends('layouts.app')

@section('title', $page->title)

@section('content')

    <a href="/contact" class="home-cta-contact">
        Let's talk!
    </a>

@endsection