@extends('layouts.app')

@section('title', $page->title)

@section('content')

    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-auto@m uk-text-center">
                <div>
                    <div class="uk-card uk-card-body"><h1>CONTACT<span class="period">.</span></h1></div>
                </div>
            </div>
                <div class="uk-width-expand@m uk-text-right uk-card uk-card-body">
                    <h2>
                        Laurens Smid
                    </h2>
                    <p>
                        Stavenissestraat 274 <br>
                        3086RM Rotterdam <br>
                        0618449885 <br>
                        info@wolf-development.nl <br>
                        <br>
                        KvK 64525155 <br>

                    </p>
                </div>
        </div>

        @if (session('success'))
            <div class="uk-alert-success" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>{{ session('success') }}</p>
            </div>
        @endif
        <form method="POST">
            {{ csrf_field() }}
            <fieldset class="uk-fieldset">
                <div class="uk-margin">
                    @if($errors->has('name'))
                    <label class="uk-form-label">Naam <br> <span class="uk-label uk-label-danger">{{$errors->first('name')}}</span></label>
                    @endif
                    <input class="uk-input" name="name" type="text" placeholder="Naam">
                </div>
                <div class="uk-margin">
                    @if($errors->has('email'))
                        <label class="uk-form-label">Naam <br> <span class="uk-label uk-label-danger">{{$errors->first('email')}}</span></label>
                    @endif
                    <input class="uk-input" name="email" type="email" placeholder="E-mail adres">
                </div>
                <div class="uk-margin">
                    @if($errors->has('message'))
                        <label class="uk-form-label">Naam <br> <span class="uk-label uk-label-danger">{{$errors->first('message')}}</span></label>
                    @endif
                    <textarea class="uk-textarea" name="message" rows="5" placeholder="Bericht"></textarea>
                </div>
                <div class="uk-margin">
                    <button class="uk-button uk-button-secondary">Versturen</button>
                </div>
            </fieldset>
        </form>

        </div>
    </div>

@endsection