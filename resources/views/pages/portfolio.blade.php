@extends('layouts.app')

@section('title', $page->title)

@section('content')

    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-auto@m uk-text-center">
                <div>
                    <div class="uk-card uk-card-body"><h1>PORTFOLIO<span class="period">.</span></h1></div>
                </div>
            </div>
        </div>
        <div class="uk-child-width-1-2@s uk-child-width-1-3@m portfolio-projects" uk-grid="masonry: true; parallax: 300">
            <div>
                <a href="https://thecooltower.nl" target="_blank"><div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 120px"><img
                                src="{{asset('images/cooltower.png')}}" alt=""></div>
                </a>
            </div>
            <div>
                <a href="https://nexthome.eu" target="_blank"><div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 120px"><img
                                src="{{asset('images/nexthome.svg')}}" alt=""></div>
                </a>
            </div>
            <div>
                <a href="https://aandebraassem.nl" target="_blank"><div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 400px"><img
                            src="https://aandebraassem.nl/images/logo_adb.svg" alt="" style="height: 100%;"></div></a>
            </div>
            <div>
                <a href="https://hurenindebrouwerij.nl" target="_blank"><div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 200px"><img
                            src="https://hurenindebrouwerij.nl/images/brouwerij-logo.svg" alt=""></div></a>
            </div>
            <div>
                <div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 150px"><img
                            src="{{asset('images/edna.png')}}" alt=""></div>
            </div>
            <div>
                <div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 130px"><img
                            src="{{asset('images/blij.png')}}" alt="">
                </div>
            </div>
            <div>
                <div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 120px"><img
                            src="{{asset('images/dvs.png')}}" alt="">

                </div>
            </div>
            <div>
                <div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 340px"><img
                            src="{{asset('images/sofyan.png')}}" alt="">
                </div>
            </div>
            <div>
                <div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 160px"><img
                            src="{{asset('images/wolf.png')}}" alt="">
                </div>
            </div>
            <div>
                <div class="uk-card uk-flex uk-flex-center uk-flex-middle" style="height: 380px"><img
                            src="{{asset('images/chefs-special.png')}}" alt=""></div>
            </div>
        </div>
    </div>

@endsection