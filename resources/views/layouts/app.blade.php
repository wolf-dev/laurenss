<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Wolf Development') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="/js/uikit/uikit.js"></script>
</head>
<body>
    <div id="app">

        <div class="uk-navbar-left" uk-sticky>
            <a class="uk-navbar-toggle" uk-toggle="target: #offcanvas-push" uk-navbar-toggle-icon></a>
        </div>
        {{--<button class="uk-button uk-button-default uk-margin-small-right" type="button" >Push</button>--}}

        <div id="offcanvas-push" uk-offcanvas="mode: push; overlay: true">
            <nav class="uk-offcanvas-bar">
                <button class="uk-offcanvas-close" type="button" uk-close></button>

                <ul class="uk-nav uk-nav-default">
                    @foreach($pages as $k => $page)
                        <li><a href="{{$page->slug}}">{{$page->title}}</a></li>
                    @endforeach
                </ul>
                <img class="uk-position-bottom uk-padding-large" src="{{asset('images/bg-full.png')}}" alt="">
            </nav>
        </div>

        @yield('content')

        {{--<footer>--}}
            {{--<img src="{{asset('images/logo-only.png')}}" alt="" class="uk-align-center">--}}
        {{--</footer>--}}
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
