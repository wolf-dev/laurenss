let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .copyDirectory('node_modules/tinymce', 'public/js/tinymce')
    .copyDirectory('node_modules/@fancyapps/fancybox', 'public/js/fancybox')
    .copyDirectory('node_modules/uikit/dist/js', 'public/js/uikit')
   .sass('resources/assets/sass/admin.scss', 'public/css')
   .sass('resources/assets/sass/app.scss', 'public/css');
