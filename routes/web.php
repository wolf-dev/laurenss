<?php


Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'namespace' => 'admin'], function() {
	Route::get('/', 'DashboardController@load');

	Route::group(['prefix' => 'paginas'], function() {
		Route::get('/', 'PageController@index');
		Route::get('/{slug}/edit', 'PageController@edit');
	});

	Route::group(['prefix' => 'afbeeldingen'], function() {
	    Route::get('/', 'ImageController@index');
	    Route::post('/upload', 'ImageController@upload');
	    Route::post('/{id}/update', 'ImageController@update');
	    Route::get('/{id}/destroy', 'ImageController@destroy');
    });

	Route::group(['prefix' => 'blocks'], function() {
	    Route::post('/{id}/update', 'BlockController@update');
	    Route::get('/{id}/change-image/{imageId}', 'BlockController@changeImage');
	    Route::get('/{id}/delete-image', 'BlockController@removeImage');
    });

	Route::group(['prefix' => 'sliders'], function() {
	    Route::get('/{id}/add-slide/{imageId}', 'SliderController@addSlide');
    });

	Route::group(['prefix' => 'slides'], function() {
	    Route::get('/{id}/change-image/{imageId}', 'SlideController@changeImage');
	    Route::get('/{id}/delete', 'SlideController@delete');
    });
});

Route::post('/contact', 'ContactSubmissionController@store');

Route::get('/', 'PageController@view');
Route::get('{slug}', 'PageController@view');