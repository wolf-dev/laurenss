<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['slider_id', 'image_id'];
    protected $with = ['image'];

    public function image()
    {
        return $this->belongsTo('App\Image');
    }
}
