<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $fillable = ['content'];

    public function block()
    {
        return $this->morphOne('App\Text', 'blockable');
    }
}
