<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['name'];
    protected $with = ['slides'];

    public function block()
    {
        return $this->morphOne('App\Slider', 'blockable');
    }

    public function slides()
    {
        return $this->hasMany('App\Slide');
    }
}
