<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['url'];

    public function block()
    {
        return $this->morphOne('App\Video', 'blockable');
    }
}
