<?php

namespace App\Http\Controllers;

use App\ContactSubmission;
use Illuminate\Http\Request;
use App\Http\Requests\StoreContactSubmissionRequest;

class ContactSubmissionController extends Controller
{
    public function store(StoreContactSubmissionRequest $request)
    {
        ContactSubmission::create($request->all());

        return back()->with('success', 'Het bericht is verzonden!');
    }
}
