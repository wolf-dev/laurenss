<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Block;

class PageController extends Controller
{
    public function view($slug = NULL)
    {
        $pages = Page::all();
        $page = Page::whereSlug("/$slug")->first();
        $blocks = Block::wherePageId($page->id)->get();

        if(!$slug) {
            $slug = 'home';
        }

        return view("pages.$slug", compact('pages', 'page', 'blocks'));
    }
}
