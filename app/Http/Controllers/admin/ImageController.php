<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\admin\ImageRequest;
use App\Http\Requests\admin\UpdateImageRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Image;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;

class ImageController extends Controller
{
    public function index()
    {
        $images = Image::all();

        return view('admin.images.index', compact('images'));
    }

    public function upload(ImageRequest $request)
    {
        $this->saveImageToDisk($request);

        Image::create($request->all());

        return redirect("/admin/afbeeldingen");
    }

    public function update($id, UpdateImageRequest $request)
    {
        $image = Image::find($id);

        if($request->hasFile('image')) {
            $this->saveImageToDisk($request);
        } else {
            if($image->name.'.'.$image->extension != $request->input('name').'.'.$request->input('extension')) {
                Storage::move('images/'.$image->name.'.'.$image->extension, 'images/'.$request->input('name').'.'.$request->input('extension'));
                Storage::move('images/thumbnails/'.$image->name.'.'.$image->extension, 'images/'.$request->input('name').'.'.$request->input('extension'));
            }
        }

        $image->update($request->all());

        return redirect("/admin/afbeeldingen");
    }

    public function destroy($id)
    {
        $image = Image::find($id);

        if($image) {
            Storage::delete(["images/$image->name.$image->extension", "images/thumbnails/$image->name.$image->extension"]);
            $image->delete();
        }

        return redirect("/admin/afbeeldingen");
    }

    public function saveImageToDisk($request)
    {
        $image = $request->file('image');
        $fileName = $request->input('name').'.'.$request->input('extension');

        $img = InterventionImage::make($image->getRealPath());
        $img->stream();

        Storage::disk('local')->put('images/'.$fileName, $img, 'public');

        $img->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->stream();

        Storage::disk('local')->put('images/thumbnails/'.$fileName, $img, 'public');
    }
}
