<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use App\Block;
use App\Image;

class PageController extends Controller
{
    public function index()
    {
        //Get all pages
        $pages = Page::all();

        return view('admin.pages.index', compact('pages'));
    }

    public function create()
    {

    }

    public function store()
    {
        //Create new page
        Page::create();
    }

    public function edit($slug)
    {
        //Get page and related blocks
        $page = Page::whereSlug($slug)->first();
        $blocks = Block::wherePageId($page->id)->get();
        $images = Image::all();

        return view('admin.pages.edit', compact('page', 'blocks', 'images'));
    }

    public function update()
    {
        //Update page
    	Page::update();
    }

    public function destroy()
    {
    	
    }
}
