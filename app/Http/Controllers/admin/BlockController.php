<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\admin\BlockRequest;
use App\Http\Controllers\Controller;
use App\Block;
use App\Text;
use App\Image;
use App\Video;
use App\Slider;
use App\Page;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;

class BlockController extends Controller
{
    public function update($id, BlockRequest $request)
    {
        $block = Block::find($id);

        //Update or create polymorphic instance
        if(!$block->blockable_id) {
            $model = "App\\".ucfirst($block->blockable_type);
            $polymorphicModel = $model::create($request->all());
            //Save new polymorphic id to block
            $block->blockable_id = $polymorphicModel->id;
            $block->save();
        } else {
            $block->blockable->update($request->all());
        }

        return back();
    }

    public function changeImage($id, $imageId)
    {
        $block = Block::find($id);
        $image = Image::find($imageId);

        if($block->blockable_type == 'image' && $image) {
            $block->blockable_id = $imageId;
            $block->save();
        }

        return back();
    }

    public function removeImage($id)
    {
        $block = Block::find($id);

        if($block->blockable_type == 'image') {
            $block->blockable_id = NULL;
            $block->save();
        }

        return back();
    }
}
