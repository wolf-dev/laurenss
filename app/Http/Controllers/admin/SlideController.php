<?php

namespace App\Http\Controllers\admin;

use App\Block;
use App\Image;
use App\Page;
use App\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    public function changeImage($id, $imageId)
    {
        $slide = Slide::find($id);
        $image = Image::find($imageId);

        if($slide && $image) {
            $slide->image_id = $imageId;
            $slide->save();
        }

        return back();
    }

    public function delete($id)
    {
        $slide = Slide::find($id);

        if($slide) {
            $slide->delete();
        }

        return back();
    }
}
