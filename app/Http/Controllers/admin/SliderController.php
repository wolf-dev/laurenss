<?php

namespace App\Http\Controllers\admin;

use App\Block;
use App\Image;
use App\Slide;
use App\Slider;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function addSlide($sliderId, $imageId)
    {
        $slider = Slider::find($sliderId);
        $image = Image::find($imageId);

        if($slider && $image) {
            Slide::create([
                'slider_id' => $sliderId,
                'image_id' => $imageId
            ]);
        }

        return back();
    }
}
