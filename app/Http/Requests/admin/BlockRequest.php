<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Block;

class BlockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $block = Block::find($this->route()->parameter('id'));

        switch($block->blockable_type) {
            case 'text':
                $rules += [
                    'content' => 'required'
                ];
                break;
            case 'image':
                $rules += [
                    'name' => 'required|unique:images,name,'.$block->blockable_id,
                    'extension' => 'required',
                    'alt' => 'required'
                ];
                break;
            case 'video':
                $rules += [
                    'url' => 'required'
                ];
                break;
        }

        return $rules;
    }
}
