<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['name', 'extension', 'alt'];

    public function block()
    {
        return $this->morphOne('App\Image', 'blockable');
    }
}
