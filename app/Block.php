<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $with = ['blockable'];

    public function blockable()
    {
        return $this->morphTo();
    }
}
